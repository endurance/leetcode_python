# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

# 方法一
class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        def reverse(prev, curr):
            if curr == None:
                return prev
            temp = curr.next
            curr.next = prev
            return reverse(curr, temp)
        return reverse(None, head)


# 方法二
class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        if head == None or head.next == None:
            return head
        last = self.reverseList(head.next)
        head.next.next = head
        head.next = None
        return last