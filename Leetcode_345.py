class Solution:

    def reverseVowels(self, s: str) -> str:

        def isVowel(ch: str) -> bool:
            return ch in "aeiouAEIOU"

        string = list(s)
        length = len(string)
        left = 0
        right = length - 1
        while left < right:
            while left < length and not isVowel(string[left]):
                left += 1
            while right > 0 and not isVowel(string[right]):
                right -= 1
            if left < right:
                temp = string[left]
                string[left] = string[right]
                string[right] = temp
                left += 1
                right -= 1
        return ''.join(string)