
class Solution(object):
    def reverseStr(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: str
        """

        l = list(s)
        for i in range(0, len(l), 2 * k):
            l[i:i+k] = reversed(l[i:i+k])
        return ''.join(l)


if __name__ == '__main__':
    list = [1, 2, 3, 4, 5]
    l1 = list[3:6]
    print(l1)