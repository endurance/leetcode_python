class Solution(object):
    def escapeGhosts(self, ghosts, target):
        """
        :type ghosts: List[List[int]]
        :type target: List[int]
        :rtype: bool
        """

        playerDistance = abs(target[0]) + abs(target[1])
        for ghost in ghosts:
            ghostDistance = abs(ghost[0] - target[0]) + abs(ghost[1] - target[1])
            if (ghostDistance <= playerDistance):
                return False
        return True


if __name__ == '__main__':
    print('hello world')